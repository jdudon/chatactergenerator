<?php

class User{

    private $id;
    private $email;
    private $password;
    private $repeatPassword;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;
        return $this;
    }

    public function setRepeatPassword($repeatPassword): ?string
    {

    }




};


?>